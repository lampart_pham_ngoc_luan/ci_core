<?php

$ci=&get_instance();
$lang=[];
$current_lang=$ci->session->userdata('language');

if(ENVIRONMENT == 'development'){
    $ci->load->model('Mlanguage','mlanguage');
    $data=$ci->mlanguage->findBy(['app_name'=>APP_NAME]);
    if($data){
        foreach($data as $item){
            $lang[$item->lang_key]=$item->{$current_lang};
        }
    }
    log_message('debug','Load language '.$current_lang.' from database but does not save cache');
}
else{
    if ( ! $lang = $ci->cache->get('db_lang_'.$current_lang))
    {
        $ci->load->model('Mlanguage','mlanguage');
        $data=$ci->mlanguage->findBy(['app_name'=>APP_NAME]);
        if($data){
            foreach($data as $item){
                $lang[$item->lang_key]=$item->{$current_lang};
            }
            // Save into the cache
            $cache_time=$ci->config->item('language_cache_time');
            if(!$cache_time){
                $cache_time='60';
            }
            $ci->cache->save('db_lang_'.$current_lang,$lang, $cache_time);
        }
        log_message('debug','Load language '.$current_lang.' from database and save cache');

    }
    else{
        log_message('debug','Load language '.$current_lang.' (database) from cache');

    }
}

//echo '<pre>';
//print_r($data);
//exit('<br/>-------------------------------------');





