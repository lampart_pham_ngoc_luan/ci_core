<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {


     public function __construct()
     {
         parent::__construct();

         $this->setTemplate('login');


     }

    public function index()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('muser');
        if($this->input->method()=='post'){

            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if($this->form_validation->run()){
                if($user=$this->muser->login($this->input->post())){
                    $this->session->set_userdata('login',$user);

                    redirect('default/common');
                }
                else{
                    $this->setMessage('Email or password incorrect !','danger');
                }
            }
            else{
                $this->setMessage('Email or password invalid !','danger');
            }

        }

        $data=['text'=>'this is test'];
        $this->view($data);
    }

    public function logout(){
         if($this->session->has_userdata('login')){
             $this->session->unset_userdata('login');
         }

         redirect('auth/login');
    }


}
