<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends MY_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        if($this->input->method()=='post'){
            $keyword=$this->input->post('keyword');
            $this->mlanguage->db->like('lang_key',$keyword);
            $language_list=$this->config->item('language_list');
            if($language_list){
                foreach($language_list as $code=>$item){
                    $this->mlanguage->db->or_like($code,$keyword);
                }
            }

            $data['data']=$this->mlanguage->findAll('',['updated_at'=>'DESC']);

        }
        else{

            $data['data']=$this->mlanguage->findAll();
        }


        $this->view($data);
    }
    public function delete($id=''){
        if((int)$id){
            $this->mlanguage->delete($id);
            $this->set_flash_message('delete success id ='.$id,'success');
            redirect('default/language');
        }
    }
    public function edit($id=''){

        if($id){
            $result=$this->mlanguage->find($id);
            $this->load->library('form_validation');
            $rules=[
                [
                    'field'=>'lang_key',
                    'label'=>'Language key',
                    'rules'=>'required',
                ],
            ];

            $this->form_validation->set_rules($rules);
            if($this->form_validation->run()){
                $post=$this->input->post();
                $user=$this->session->userdata('login');
                $post['updated_at']=date('Y-m-d H:i:s');
                $post['user_id']=isset($user->id)?$user->id:0;
                $this->mlanguage->edit($id,$post);
                $this->set_flash_message('saved','success');
                redirect('default/language');
            }
        }
        $data['data']=$result;
       $this->view($data);
    }

    public function change_language($code){
        $language_list=$this->config->item('language_list');
        $codes=array_keys($language_list);
        if(in_array($code,$codes)){
            $this->session->set_userdata('language',$code);
        }

        redirect($_SERVER['HTTP_REFERER']);

    }

    public function clear_cache(){
        $this->cache->delete('db_lang_'.$this->session->userdata('language'));
        $this->set_flash_message('Clear cache successfull');
        redirect($_SERVER['HTTP_REFERER']);
    }
}
