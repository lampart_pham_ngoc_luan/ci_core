<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('Language','language')?>

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> <?php echo lang('Language','language')?></a></li>
            <li class="active"><?php echo lang('edit','label_edit')?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-md-6">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo lang('Edit language','label_edit_language')?></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php echo $message ?>
                    <form action="" class="form-horizontal" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Language key</label>

                                <div class="col-sm-10">
                                    <input type="text" name="lang_key" value="<?php echo $data->lang_key?>" class="form-control" readonly="readonly" id="inputEmail3" >
                                </div>
                            </div>
                            <?php
                            $lang_list=$this->config->item('language_list');
                            if($lang_list){
                                foreach($lang_list as $lang_code=>$lang_name){ ?>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label"><?php echo ucfirst($lang_name) ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="<?php echo $lang_code?>" value="<?php echo $data->$lang_code?>" class="form-control" id="inputPassword3"
                                                   placeholder="Password">
                                        </div>
                                    </div>
                                <?php }
                            }
                            ?>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">

                            <button type="submit" class="btn btn-info "><?php echo lang('Save','button_save')?></button>&nbsp;&nbsp;
                            <a class="btn btn-default"  href="<?php echo base_url('default/language') ?>"><?php echo lang('Back','button_back')?></a>

                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->