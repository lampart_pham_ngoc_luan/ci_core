<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo lang('Language','language')?>

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> <?php echo lang('Language','language')?></a></li>
            <li class="active"><?php echo lang('list','list')?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo lang('List item','list_item')?></h3>

                        <div class="row">
                           <form action="" method="post">
                               <div class="col-sm-3">
                                   <input name="keyword" value="<?php echo $this->input->post('keyword') ?>" class="form-control" placeholder="keyword"/>
                               </div>
                               <div class="col-sm-3">
                                   <input type="submit" class="btn btn-primary" value="Search"/>
                               </div>
                           </form>

                            <div class="col-sm-1 pull-right">
                                <a onclick="return confirm('Do you want to clear cache ?')" href="<?php echo base_url('default/language/clear_cache') ?>" class="btn btn-default pull-right"/>Clear cache</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <?php echo $message ?>
                        <?php
                        $lang_list=$this->config->item('language_list');

                        ?>
                        <table class="table table-hover">
                            <tr>
                                <th>ID</th>
                                <th>App name</th>
                                <th>Lang key</th>
                                <?php if($lang_list){
                                    foreach($lang_list as $lang_code=>$lang_name){
                                        echo '<th>'.$lang_name.'</th>';
                                    }
                                }?>

                                <th>created at</th>
                                <th>created at</th>
                                <th>action</th>
                            </tr>

                            <?php if($data){
                                foreach($data as $item){ ?>
                                    <tr>
                                        <td><?php echo $item->id ?></td>
                                        <td><?php echo $item->app_name ?></td>
                                        <td><?php echo $item->lang_key ?></td>
                                        <?php if($lang_list){
                                            foreach($lang_list as $lang_code=>$lang_name){
                                                echo '<td>'.$item->{$lang_code}.'</td>';
                                            }
                                        }?>

                                        <td><?php echo $item->created_at ?></td>
                                        <td><?php echo $item->updated_at ?></td>
                                        <td>
                                            <a href="<?php echo base_url('default/language/edit/'.$item->id) ?>"><i class="fa fa-edit"></i></a> &nbsp;&nbsp;
                                            <a onclick="return confirm('Do you want to delete this item ?')" href="<?php echo base_url('default/language/delete/'.$item->id) ?>" class="text text-danger"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                <?php }
                            }?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->