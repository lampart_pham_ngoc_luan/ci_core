<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    echo lang('this is English','this_is_english');
	    echo '<br/>';
        echo 'welcome test controller';
	}

	public function test_helper(){
	    $this->load->helper('example');
	    test();
    }

    public function test_library(){
        $this->load->library('Example');
        $this->example->test();
    }

    public function test_language(){
        $this->lang->load('example');
        echo '<pre>';
        print_r($this->lang);
        exit('<br/>-------------------------------------');
    }

    public function test_smarty(){
        $this->smarty->view('example');
    }
}
