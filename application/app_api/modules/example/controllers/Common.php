<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'/libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

/*
 * Example module api
 */
class Common extends REST_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('mapiuser');

    }
    /*
     * url: domain.com/account/user/list
     * method : get
     * response type : json
     */

    public function list_get()
    {
        $users=$this->mapiuser->findAll();

        $this->response($users, 200);
    }

    /*
     * url: domain.com/account/user/create
     * method : post
     * response type : json
     */

    public function create_post()
    {
        $data['username']=$this->post('username');
        $data['email']=$this->post('email');
        $data['password']=encrypt_password($this->post('password'));
        $data['token']='';
        $data['token_expire']=date('Y-m-d H:i:s');;
        $data['created_at']=date('Y-m-d H:i:s');
        $data['updated_at']=date('Y-m-d H:i:s');
        if(!$data['username'] || !$data['email'] || !$data['password']){
            $this->response(['message'=>'data invalid'],200);
        }
        else{
            if($this->mapiuser->add($data)){
                $this->response(['message'=>'create user success'],200);
            }
            else{
                $this->response(['message'=>'create user faid'],200);
            }
        }


    }

    /*
     * url: domain.com/account/user/update/id/1
     * method : post
     * response type : json
     */

    public function update_post()
    {
        $data['id']=$this->get('id');
        $data['data']=$this->post();
        //do somthing for update item
        $this->response($data, 200);
    }

    /*
     * url: domain.com/account/user/delete/id/1
     * method : get
     * response type : json
     */

    public function delete_get()
    {
        $data['id']=$this->get('id');

        //do somthing for delete item
        $this->response($data, 200);
    }
}
?>