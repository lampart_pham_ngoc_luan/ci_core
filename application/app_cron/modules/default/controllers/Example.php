<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Example module api
 */
class Example extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
    }


    /*
     * url: cron.domain.com/default/example/
     * method : get
     * response type : json
     */
    public function index()
    {
       //do something
        echo 'cron job ready';
    }


}
?>