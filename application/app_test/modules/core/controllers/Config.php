<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Example module api
 */
class Config extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }


    /*
     * url: cron.domain.com/default/example/
     * method : get
     * response type : json
     */
    public function index()
    {
        $this->unit->use_strict(TRUE);
        $this->unit->run(1, 1);


        echo $this->unit->report();
    }


}
?>