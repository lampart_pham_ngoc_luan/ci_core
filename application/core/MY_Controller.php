<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class MY_Controller extends MX_Controller {
    protected $template;
    protected $theme;
    protected $view;
    protected $message;
    public function __construct() {
        parent::__construct();

        $this->theme=$this->config->item('theme');
        $this->template=$this->config->item('template');


        $this->module=$this->router->fetch_module();
        $this->class=$this->router->fetch_class();
        $this->method=$this->router->fetch_method();

        $this->view=$this->method;

        $this->get_flash_message();

    }

    public function view($data=''){
        $template='template/'.APP_NAME.'/'.$this->theme.'/'.$this->template;

        //set view load
        $data['view']=$this->router->fetch_class().'/'.$this->view;
        //check view file exists
        if($this->module !='auth'){
            $view_path=APPPATH.'app_'.APP_NAME.'/modules/'.$this->module.'/views/'.$this->class.'/'.$this->view.'.php';
            if(!file_exists($view_path)){
                exit('not found view : '.$view_path);
            }
        }

        $data['message']=$this->generate_message();


        $data['data']=$data;

        $this->load->view($template,$data['data']);
    }

    protected function setView($view){
        $this->view=$view;
    }

    protected function setTemplate($template){
        $this->template=$template;
    }

    protected function setTheme($theme){
        $this->theme=$theme;
    }

    /*
      * function : set_flash_message
      * is used when you want transport a message from current page to other page
      * @param (string) $message is message
      * @param (string) $type , value is : info (alert information),warning (alert warning something),danger( alert error),success(alert successfull action)
      * @return  set message session;
      * date : 2017-04-20
      * author : Mr Luan
      */
    public function set_flash_message($message,$type='info') {
        if(!$message){
            log_message('error','$message in set_flash_message function can not null');
        }
        $data=[
            'type'=>$type,
            'message'=>$message,
        ];
        $this->session->set_userdata('message', $data);
    }

    /*
     * function :get_flash_message
     * is used get flash message is transfer from referrer page
     * function will autoload in _construct function,
     * @param null
     * @return  $message for view;
     * date : 2017-04-20
     * author : Mr Luan
     */

    protected function get_flash_message() {
        if ($this->session->userdata('message')) {
            $this->message[] = $this->session->userdata('message');
            $this->session->unset_userdata('message');
//            $this->generate_message();
        }

        return ;
    }

    /*
    *function: set_message
    * @param (string) $message is message
    * @param (string) $type , value is : info (alert information),warning (alert warning something),danger( alert error),success(alert successfull action)
    * @return  $message in view;
    * date : 2017-04-20
    * author : Mr Luan
    */
    public function set_message($message, $type='info') { //$type : info, warning,danger, success
        if(!$message){
            log_message('error','$message in set_message function can not null');
        }
        $this->message[]=[
            'type'=>$type,
            'message'=>$message,
        ];
//        $this->generate_message();

    }

    /*
     * function :generate_message
     * make html for $message and assign for view
     * @param null
     * @return  $message in view;
     * date : 2017-04-20
     * author : Mr Luan
     */

    protected function generate_message(){

        $icon=[
            'info'=>'info-circle',
            'warning'=>'warning',
            'danger'=>'minus-circle',
            'success'=>'check-circle',
        ];

        $types=['info','warning','danger','success'];
        $html='';
        if($this->message){
            foreach($this->message as $m){
                if(!in_array($m['type'],$types)){
                    log_message('error','generate_message(), $type = '.$m['type'].' invalid');
                    continue;
                }
                $html.='<div class="col-sm-12"><div class="global-message  alert alert-'.$m['type'].' alert-dismissable"><i class="fa fa-'.(isset($icon[$m['type']])?$icon[$m['type']]:'').' "></i>&nbsp;&nbsp;'.$m['message'].'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></div></div>';
            }
        }
        return $html;
    }
}