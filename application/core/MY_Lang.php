<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
//require APPPATH."third_party/MX/Lang.php";
class MY_Lang extends MX_Lang
{
    public function load($langfile, $lang = '', $return = FALSE, $add_suffix = TRUE, $alt_path = '', $_module = '')
    {

        if (is_array($langfile))
        {
            foreach($langfile as $_lang) $this->load($_lang);
            return $this->language;
        }

        $deft_lang = CI::$APP->config->item('language');
        $idiom = ($lang == '') ? $deft_lang : $lang;


        if (in_array($langfile.'_lang'.EXT, $this->is_loaded, TRUE))
            return $this->language;

        //load general language
        $path=APPPATH.'language/'.$idiom.'/'.$langfile.'_lang'.EXT;
        if(file_exists($path)){
            CI_Lang::load($langfile, $lang, $return, $add_suffix, $alt_path);
        }

        //load app language
        list($path, $_helper) = $this->findFile($langfile.'_lang','language/'.$idiom);
        if ($path !== FALSE) {

            $lang_app=$this->loadLangApp($path);
            if ($lang_app){
                $this->language = array_merge($this->language, $lang_app);
                $this->is_loaded[] = $langfile.'_lang'.EXT;
                unset($lang_app);
            }
        }


        //load module language
        $_module OR $_module = CI::$APP->router->fetch_module();
        list($path, $_langfile) = Modules::find($langfile.'_lang', $_module, 'language/'.$idiom.'/');
        if ($path !== FALSE)
        {
            if($lang_module = Modules::load_file($_langfile, $path, 'lang'))
            {
                if ($return) return $lang_module;
                $this->language = array_merge($this->language, $lang_module);
                $this->is_loaded[] = $langfile.'_lang'.EXT;
                unset($lang_module);
            }

        }


        return $this->language;
    }

    private function loadLangApp($path){
        include $path;
        return $lang;
    }

    private function findFile($file,$folder){

        $segments = explode('/', $file);

        $file = array_pop($segments);
        $file_ext = (pathinfo($file, PATHINFO_EXTENSION)) ? $file : $file.EXT;

        $path = APPPATH.'app_'.APP_NAME.'/'.$folder.'/';

        $fullpath=$path.$file_ext;

        if(is_file($fullpath) && file_exists($fullpath)){
            return array($fullpath, $file);
        }

        return array(FALSE, $file);
    }
}