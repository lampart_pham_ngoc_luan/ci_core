<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";


class MY_Loader extends MX_Loader {


    /** Load a module library **/
    public function library($library, $params = NULL, $object_name = NULL)
    {

        if (is_array($library)) return $this->libraries($library);

        $class = strtolower(basename($library));

        if (isset($this->_ci_classes[$class]) && $_alias = $this->_ci_classes[$class])
            return $this;

        ($_alias = strtolower($object_name)) OR $_alias = $class;

        list($path, $_library) = Modules::find($library, $this->_module, 'libraries/');

        /* load library config file as params */
        if ($params == NULL)
        {
            list($path2, $file) = Modules::find($_alias, $this->_module, 'config/');

            ($path2) && $params = Modules::load_file($file, $path2, 'config');
        }

        if ($path === FALSE)
        {

            list($path, $_library)=$this->findFile($library,'libraries');

            if($path === FALSE){
                $this->_ci_load_library($library, $params, $object_name);
            }
            else{

                include_once $path;


                $library = ucfirst($_library);
                CI::$APP->$_alias = new $library($params);
                $this->_ci_classes[$class] = $_alias;

            }

        }
        else
        {
            Modules::load_file($_library, $path);

            $library = ucfirst($_library);
            CI::$APP->$_alias = new $library($params);

            $this->_ci_classes[$class] = $_alias;
        }
        return $this;
    }

    /** Load a module helper **/
    public function helper($helper = array())
    {
        if (is_array($helper)) return $this->helpers($helper);

        if (isset($this->_ci_helpers[$helper]))	return;

        list($path, $_helper) = Modules::find($helper.'_helper', $this->_module, 'helpers/');

        if ($path === FALSE) {
            list($path, $_helper) = $this->findFile($helper.'_helper','helpers');

            if ($path === FALSE) {
                return parent::helper($helper);
            }
            else{
                include_once  $path;
                $this->_ci_helpers[$_helper] = TRUE;
                return $this;
            }

        }

        Modules::load_file($_helper, $path);
        $this->_ci_helpers[$_helper] = TRUE;
        return $this;
    }


    private function findFile($file,$folder){

        $segments = explode('/', $file);

        $file = array_pop($segments);
        if($folder == 'libraries'){
            $file=ucfirst($file);
        }

        $file_ext = (pathinfo($file, PATHINFO_EXTENSION)) ? $file : $file.EXT;

        $path = APPPATH.'app_'.APP_NAME.'/'.$folder.'/';

        $fullpath=$path.$file_ext;

       if(is_file($fullpath) && file_exists($fullpath)){
           return array($fullpath, $file);
       }

        return array(FALSE, $file);
    }


}