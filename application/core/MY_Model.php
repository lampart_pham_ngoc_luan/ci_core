<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php

//code by mrluan ngocluantdt@gmail.com
class MY_Model extends CI_Model {

    public $table;
    public $join='';
    public $select='';
    public $like='';
    public $group_by;

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function find($id,$call_back=true) {
        if ((int) $id) {
            $this->db->where($this->table.'.id', $id);

            if($call_back){
                $this->callBack();
            }
            
            $query = $this->db->get($this->table);
            return $query->row();
        }
    }

    public function findAll($order='',$call_back=true) {
        if($order && is_array($order)){
            foreach($order as $key=>$item){
                $this->db->order_by($key,$item);
            }
        }
        if($call_back){
                $this->callBack();
            }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function findOneBy($where = array(),$call_back=true) {
        if ($where != '') {
            $this->db->where($where);
        } else
            exit('not found id');
         if($call_back){
                $this->callBack();
            }
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function findBy($where='',$order='',$limit='',$call_back=true) {
        if ($where) {
            $this->db->where($where);
        } 
        
        if($order && is_array($order)){
            foreach($order as $key=>$item){
                $this->db->order_by($key,$item);
            }
        }
          if($call_back){
                $this->callBack();
            }
        if($limit){
            $this->db->limit($limit);
        }
        
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function add($data = array()) {
        if ($data != '') {
            
            $this->db->insert($this->table, $data);
            return $this->db->insert_id();
        } else
            exit('not found data, add fail');
    }

    public function edit($id, $data = '') {
        if ($data != '' && $id != '') {
    
            $this->db->where($this->table.'.id', $id);
            $this->db->update($this->table, $data);
        } else
            exit('not found data or id, edit fail');
    }
    public function editBy($where='', $data ='') {
        if ($data != '' && $where != '') {
            $this->db->where($where);
            $this->db->update($this->table, $data);
        } else
            exit('not found data or condition, edit fail');
    }

    public function delete($id) {
        if(is_array($id)){
            $this->db->where('id in ('.implode(',', $id).')');
            $this->db->delete($this->table);
        }
        else if ((int) $id) {
            $this->db->where('id', $id);
            $this->db->delete($this->table);
        } else
            exit('id can not null');
    }
    
     public function deleteBy($where='') {
         if($where){
             $this->db->where($where);
             
         }
         $this->db->delete($this->table);
       
    }
    
    public function query($sql){
        $query=$this->db->query($sql);
        return $query;
    }
    
    public function getSql(){
        return $this->db->last_query();
    }
    
    
    public function countAll($where = array()) {
        if ($where) {
            $this->db->where($where);
        }
        return $this->db->count_all_results($this->table);
    }
    
    protected function callCondition($condition){
        if($condition){
            foreach($condition as $function =>$item){
                if($item){
                    switch ($function){
                        case 'group_by':$this->db->$function($item);break;
                        default : {
                            foreach($item as $k=>$v){
                                $this->db->$function($k,$v);
                            };
                        }
                    }
                }
            }
        }
    }
    public function pagination($condition='',$number='',$offset='',$count=false,$call_back=true){
               
        $this->callCondition($condition);
        if($call_back){
                $this->callBack();
            }
        
        if($number!=''){
            $d=$this->db->get($this->table,$number,$offset);
        }
        else{
            $d=$this->db->get($this->table);
        }
 
        if($count==true){
            return $d->num_rows();
        }
        else{
            return $d->result();
        }
        
    }
    
    public function callBack(){
        return true;
    }
    
}