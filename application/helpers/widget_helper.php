<?php

if(!function_exists('public_url')){
    function public_url($url=''){
        return base_url().'public/'.APP_NAME.'/'.$url;
    }
}

function show_message($mesage){
    if($mesage){
        foreach($mesage as $item){
            echo '<p class="alert alert-'.$item['type'].'">'.$item['message'].'</p>';
        }
    }
}

function encrypt_password($password){
    $ci=&get_instance();
    $encryption_key=$ci->config->item('encryption_key');
    $options = [
        'salt' =>$encryption_key,
    ];
    return password_hash($password, PASSWORD_BCRYPT, $options);
}


function lang($lang_default='',$key='',$cache=true){
    if(!$lang_default){
        $lang_default='not declare';
    }
    if(!$key){
        $key=md5($lang_default);
    }

    if(ENVIRONMENT == 'development'){
        $ci=&get_instance();
        $ci->load->model('Mlanguage','mlanguage');

        if(!$ci->mlanguage->countAll(['lang_key'=>$key,'app_name'=>APP_NAME])){
            $default_language=$ci->config->item('language_add');
            $add=[
                'lang_key'=>$key,
                $default_language=>$lang_default,
                'app_name'=>APP_NAME,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
            ];
            $ci->mlanguage->add($add);
        }
    }


    return  $ci->lang->line($key);

}
