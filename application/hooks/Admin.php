<?php
/**
 * Created by PhpStorm.
 * User: ngoc_luan
 * Date: 4/5/2017
 * Time: 11:46 AM
 */

Class Admin{
    protected $ci;
    public function __construct()
    {
        $this->ci=get_instance();
    }

    public function run($param){
        $this->init($param);
        $this->checkAuth();

        //show debug tool bottom of page
        $this->debugTool();
    }

    protected function init($param){

        $this->ci->load->library('smartie','','smarty');

        if(!$this->ci->session->has_userdata('language')){
            $this->ci->session->set_userdata('language',$this->ci->config->item('language'));
        }

        //load cache
        $this->ci->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

        $this->ci->lang->load('database');


    }

    protected function debugTool(){
        if(ENVIRONMENT != 'development'){
            return false;
        }

        if(!$this->ci->config->item('enable_profiler')){
            return false;
        }

        if($this->ci->input->is_ajax_request() || $this->ci->input->is_cli_request()){
            return false;
        }


        $this->ci->output->enable_profiler(TRUE);
        $this->ci->load->library('profiler');
    }


    protected function checkAuth(){

        if($this->ci->router->fetch_module()!='auth'){
            if(!$this->ci->session->has_userdata('login')){
                redirect('auth/login');
            }
        }
    }



}