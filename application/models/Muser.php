<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Muser extends MY_Model
{
    public $table= 'user';


    public function login($input){
        $this->db->where('email',$input['email']);
        $this->db->or_where('username',$input['email']);
        $data=$this->db->get($this->table);

        if($data->num_rows()){
            $result=$data->row();

            $password=encrypt_password($input['password']);

            if($password==$result->password){
               unset($result->password);
                return $result;
            }
        }
        return false;
    }


}