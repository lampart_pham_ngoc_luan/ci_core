<?php
/* Smarty version 3.1.30, created on 2017-04-10 11:27:56
  from "/var/htdocs/ci_core/application/views/templates/header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58eb09cc6f12e4_36615062',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c6d11b16ba658fc44f4d824376385a3ea20d866b' => 
    array (
      0 => '/var/htdocs/ci_core/application/views/templates/header.tpl',
      1 => 1464121800,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58eb09cc6f12e4_36615062 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<!--[if ie 6]><![endif]-->

<head>
	<meta charset="utf-8" />
	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 - <?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</title> 

	<meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Mono:400,400italic' rel='stylesheet' type='text/css'>

	<style type="text/css">
	
		body {background-color: #fff; color: #000; width: 800px; font-family: 'Roboto Mono', 'courier', 'courier new', monotype;}
		h1, h2 {background-color: #fff; color: #999; font-family: 'Roboto Slab', 'Times New Roman', Georgia, serif;}
		h1 {font-size: 2em;}
		h2 {font-size: 1.5em;}
		em {border: solid #000 1px; padding: 0 5px; font-style: normal;}
		label {display: inline-block; width: 10em; text-align: right;}
		input[type="submit"] {margin: 1em 0 0 10em;}
		.error {background-color: #ff0; color: #c00;}
		.message {background-color: #fff; color: #0c0;}
	
	</style>
</head> 
<body>
<?php }
}
