<?php
/**
 * Created by PhpStorm.
 * User: ngoc_luan
 * Date: 4/10/2017
 * Time: 11:55 AM
 */

$this->load->view('template/'.APP_NAME.'/adminlte/header');
$this->load->view('template/'.APP_NAME.'/adminlte/navigation');
$this->load->view($view);

$this->load->view('template/'.APP_NAME.'/adminlte/footer');