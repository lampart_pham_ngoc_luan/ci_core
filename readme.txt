/**** Rease NOTE *****/

Current version : 0.8

- Giải pháp: Requests sẽ đi qua file index.php,  trong file index sẽ detect theo subdomain và chuyển hướng request đến thư mục app tương ứng với subdomain đó theo đường dẫn application/app_[subdomain_name], nếu không tìm thấy thư mục app tương ứng, request sẽ đi đến thư mục app mặc định là application/app_site, do vậy, các ứng dụng api, admin, cron sau này sẽ được setup 1 sub domain theo thứ tự api.domain.com, admin.domain.com, cron.domain.com . Mỗi app sẽ là một mô hình HMVC .

- Core CI framework được phát triển từ Codeigniter 3.1.4 (mới nhất tính đến ngày 2017-04-05 )
- Struction : Sử dụng hmvc third party  https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc
- API : Sử dụng library resful cho API : https://github.com/chriskacerguis/codeigniter-restserver
- View : có thể sử dụng được 2 hình thức: template thuần và template smarty, template smarty sử dụng thư viện : http://www.coolphptools.com/codeigniter-smarty

- Định nghĩa các folder
  + application/*  gọi là vùng global hoặc global
  + application/app_[app name]/*  : gọi là vùng app hoặc app
  + application/app_[app_name]/modules/[module name]/*  : gọi là vùng module hoặc module

- Libraries, helpers : Mỗi vùng đều có libraries, helpers được load thứ tự ưu tiên module-app-global, ví dụ có 2 Library cùng tên, 1 nằm trong vùng app và 1 trong vùng module, thì system sẽ load library nằm trong vùng Module và bỏ qua library vùng App
- Config ,route và autoload :  Riêng config ,route và autoload, vùng app chỉ có 1 file cố định là config.php, routes.php và autoload.php, các biến trong các file config này sẽ override các biến trong config của global, 2 vùng còn lại tương tự nhau
- Language :  nếu vùng nào có file thỏa điệu kiện load (có file cần load) sẽ load file đó,  nếu 3 vùng đều tồn tại 3 file  trùng tên sẽ load cả 3 file, dữ liệu language sẽ được merge lại, nếu 3 file có 3 biến trùng tên nhau, vùng bên trong sẽ overide vùng bên ngoài
- Hook : mỗi app sẽ có 1 hook trong thư mục applidation/hooks, sẽ được tự động gọi đế hàm run tương ứng với hook của app đang active, có thể xem và sửa đổi khai báo hook trong application/config/hooks.php

- Quản lý vendor bằng composer, chuyển thu mục system của codeigniter vào vendor và đổi tên thành codeigniter314

- Đã code login cho admin.domain.com